import styles from './Loading.module.css'

const LoadingPage = () => {
  return (
    <label className={styles['container']}>Loading...</label>
  )
}

export default LoadingPage
