import { lazy } from 'react';
import { RouteProps } from 'react-router-dom'

const lazyComponent = (page: string) => {
  /* @vite-ignore */
  const Component = lazy(() => import(`./pages/${page}/index.ts`))
  return () => <Component />
}

const routes: RouteProps[] = [
  { path: '/', exact: true, component: lazyComponent('Home') },
  { path: '/', component: lazyComponent('FileNotFound') },
]

export default routes
