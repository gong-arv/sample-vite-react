FROM node:16-alpine3.12 as builder
WORKDIR /src

COPY package.json yarn.lock ./
RUN yarn

COPY . .
RUN yarn build

FROM nginx:1.21.3-alpine
WORKDIR /usr/share/nginx/html

RUN sed -i '10i\        try_files $uri $uri\/ \/index.html;\' /etc/nginx/conf.d/default.conf

COPY --from=builder /src/dist/ ./
